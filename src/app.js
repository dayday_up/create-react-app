import React from 'react';
import Home from './views/Home';
import './app.less';

const App = () => {
  return(
      <div>
        <Home/>
      </div>
  );
};

export default App;