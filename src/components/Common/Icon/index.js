import React from 'react';
import './fonts/iconfont.css';
import './style.less';

const Icon = (props) => {
  return(
      <i className={`iconfont ${props.classes}`} />
  )
};

export default Icon;